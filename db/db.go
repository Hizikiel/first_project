package db

import "project_second/models"

type Database struct {
	users map[string]models.User
}

func NewDatabase() *Database {
	users := make(map[string]models.User)
	return &Database{
		users: users,
	}
}

func (db *Database) SaveUser(user models.User) {
	db.users[user.ID] = user
}

func (db *Database) GetUserByID(userID string) (models.User, bool) {
	user, ok := db.users[userID]
	return user, ok
}

func (db *Database) GetUserByUsername(username string) (models.User, bool) {
	for _, user := range db.users {
		if user.Username == username {
			return user, true
		}
	}
	return models.User{}, false
}

func (db *Database) GetAllUsers() []models.User {
	var users []models.User
	for _, user := range db.users {
		users = append(users, user)
	}
	return users
}
