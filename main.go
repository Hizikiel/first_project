package main

import (
	"fmt"
	"log"
	"project_second/handlers"
	"project_second/middleware"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.Use(middleware.Timeout())
	router.POST("/register", handlers.RegisterHandler)
	router.GET("/login", handlers.LoginHandler)
	router.GET("/user", handlers.ListUser)
	router.POST("/upload", handlers.UploadImageHandler)
	router.GET("/open/:image_name", handlers.OpenImageHandler)

	port := ":8080"
	fmt.Printf("Server is running on port %s\n", port)
	if err := router.Run(port); err != nil {
		log.Fatalf("Failed to start server: %v", err)
	}
}
