package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func UploadImageHandler(c *gin.Context) {
	file, err := c.FormFile("image")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "No file uploaded"})
		return
	}

	filename := "images/" + file.Filename
	if err := c.SaveUploadedFile(file, filename); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to save file"})
		return
	}

	println("Uploaded file:", filename)

	c.JSON(http.StatusOK, gin.H{"message": "File uploaded successfully"})
}
