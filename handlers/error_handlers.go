package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func ErrorHandler() gin.HandlerFunc {

	return func(c *gin.Context) {
		c.Next()

		err := c.Errors.Last()
		if err != nil {
			switch err.Type {
			case gin.ErrorTypeBind:
				c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request parameters"})
			case gin.ErrorTypeRender:
				c.JSON(http.StatusInternalServerError, gin.H{"error": "Internal server error"})
			default:
				c.JSON(http.StatusInternalServerError, gin.H{"error": "An unexpected error occurred"})
			}
		}
	}
}
