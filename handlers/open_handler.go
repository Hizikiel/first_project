package handlers

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func OpenImageHandler(c *gin.Context) {
	imageName := c.Param("image_name")

	file, err := os.Open("images/" + imageName)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Image not found"})
		return
	}
	defer file.Close()

	c.File("images/" + imageName)
}
