package db

import "project1/model"

var users map[string]model.User

func init() {
	users = make(map[string]model.User)
}

func AddUser(user model.User) {
	users[user.Username] = user
}

func GetUser(username string) (model.User, bool) {
	user, ok := users[username]
	return user, ok
}

func List() map[string]model.User {
	return users
}
