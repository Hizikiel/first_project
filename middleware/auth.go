package middleware

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func RequestIDMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		requestID := GenerateRequestID()

		ctx := WithRequestID(c.Request.Context(), requestID)
		c.Request = c.Request.WithContext(ctx)

		c.Next()
	}
}

func UserIDMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		userID := "123"

		ctx := WithUserID(c.Request.Context(), userID)
		c.Request = c.Request.WithContext(ctx)

		c.Next()
	}
}

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		authToken := c.GetHeader("Authorization")
		if authToken != "valid_token" {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
			return
		}

		c.Next()
	}
}

func ProtectedHandler(c *gin.Context) {
	requestID := GetRequestID(c.Request.Context())
	userID := GetUserID(c.Request.Context())

	c.JSON(http.StatusOK, gin.H{
		"message":    "Protected endpoint accessed!",
		"request_id": requestID,
		"user_id":    userID,
	})
}
func GenerateRequestID() string {
	return fmt.Sprintf("%d", time.Now().UnixNano())
}

type requestIDKey struct{}
type userIDKey struct{}

func WithRequestID(ctx context.Context, requestID string) context.Context {
	return context.WithValue(ctx, requestIDKey{}, requestID)
}
func GetRequestID(ctx context.Context) string {
	requestID, _ := ctx.Value(requestIDKey{}).(string)
	return requestID
}
func WithUserID(ctx context.Context, userID string) context.Context {
	return context.WithValue(ctx, userIDKey{}, userID)
}
func GetUserID(ctx context.Context) string {
	userID, _ := ctx.Value(userIDKey{}).(string)
	return userID
}
